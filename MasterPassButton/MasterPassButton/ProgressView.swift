//
//  ProgressView.swift
//  way2go
//
//  Created by Leandro Gava Destro on 20/07/17.
//  Copyright © 2017 Informant. All rights reserved.
//

import Foundation
import UIKit


open class ProgressView {
    
    private var containerView = UIView()
    private var progressView = UIView()
    private var activityIndicator = UIActivityIndicatorView()
    
    open class var shared: ProgressView {
        struct Static {
            static let instance: ProgressView = ProgressView()
        }
        return Static.instance
    }
    
    open static func showProgressView() {
        
        let targetView = ProgressView.currentViewController()
        
        shared.containerView.frame = (targetView?.view.frame)!
        shared.containerView.center = (targetView?.view.center)!
        shared.containerView.backgroundColor = UIColor.color(hex: "222222", alpha: 0.4)
        
        shared.progressView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        shared.progressView.center = (targetView?.view.center)!
        shared.progressView.backgroundColor = UIColor.color(hex: "444444", alpha: 0.7)
        shared.progressView.clipsToBounds = true
        shared.progressView.layer.cornerRadius = 10
        
        shared.activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        shared.activityIndicator.activityIndicatorViewStyle = .whiteLarge
        shared.activityIndicator.center = CGPoint(x: shared.progressView.bounds.width / 2, y: shared.progressView.bounds.height / 2)
        
        shared.progressView.addSubview(shared.activityIndicator)
        shared.containerView.addSubview(shared.progressView)
        shared.activityIndicator.startAnimating()
        
        shared.containerView.alpha = 0
        targetView?.view.addSubview(shared.containerView)
        
        UIView.animate(withDuration: 0.33, animations: {
            shared.containerView.alpha = 1
        }) { _ in }
    }
    
    open static func hideProgressView(animated: Bool = true) {
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                shared.containerView.alpha = 0
            }) { _ in
                shared.activityIndicator.stopAnimating()
                shared.containerView.removeFromSuperview()
            }
        } else {
            shared.containerView.alpha = 0
            shared.activityIndicator.stopAnimating()
            shared.containerView.removeFromSuperview()
        }
    }
    
    static func currentViewController() -> UIViewController? {
        
        let navController = UIApplication.shared.keyWindow?.rootViewController
        var visibleView: UIViewController? = nil
        
        if let navigationController: UINavigationController = navController as? UINavigationController {
            visibleView = navigationController.visibleViewController
        } else if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            
            var currentController: UIViewController! = rootController
            var attempts = 0
            
            while(currentController.presentedViewController != nil && attempts < 20) {
                currentController = currentController.presentedViewController
                attempts = attempts + 1
            }
            
            return currentController
        }
        
        return visibleView
    }
    
}




extension UIColor {
    
    static func color(hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).uppercased()
        
        if cString.hasPrefix("#") {
            cString = String(NSString(string: cString).substring(from: 1))
        }
        
        if cString.characters.count != 6 {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red   : CGFloat((rgbValue & 0xFF0000 ) >> 16) / 255.0 ,
            green : CGFloat((rgbValue & 0x00FF00 ) >> 8 ) / 255.0 ,
            blue  : CGFloat( rgbValue & 0x0000FF )        / 255.0 ,
            alpha : CGFloat(1.0)
        )
    }
    
    static func color(hex: String, alpha: Float) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).uppercased()
        
        if cString.hasPrefix("#") {
            cString = String(NSString(string: cString).substring(from: 1))
        }
        
        if cString.characters.count != 6 {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red   : CGFloat((rgbValue & 0xFF0000 ) >> 16) / 255.0 ,
            green : CGFloat((rgbValue & 0x00FF00 ) >> 8 ) / 255.0 ,
            blue  : CGFloat( rgbValue & 0x0000FF )        / 255.0 ,
            alpha : CGFloat(alpha)
        )
    }
    
}
