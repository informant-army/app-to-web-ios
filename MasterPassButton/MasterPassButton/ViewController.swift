//
//  ViewController.swift
//  MasterPassButton
//
//  Created by Leandro Gava Destro on 10/10/2017.
//  Copyright © 2017 Informant. All rights reserved.
//

import UIKit
import SafariServices


var _viewController: ViewController? = nil


class ViewController: UIViewController {

    @IBOutlet weak var mpButtonWebView: UIWebView!
    
    var safariView : SFSafariViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _viewController = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        ProgressView.showProgressView()
        loadWebViewContent()
    }
    
    func loadWebViewContent() {
        mpButtonWebView?.backgroundColor = .clear
        mpButtonWebView?.isOpaque = false
        mpButtonWebView?.loadHTMLString(ViewController.masterpassButtonHTML(), baseURL: nil)
        mpButtonWebView?.delegate = self
    }

}

extension ViewController: UIWebViewDelegate {
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if (request.url?.scheme == "inapp") {
            self.initializeBuyWithMasterPass()
        }
        return true;
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        ProgressView.hideProgressView()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.loadWebViewContent()
        ProgressView.hideProgressView(animated: true)
    }
    
}

extension ViewController: SFSafariViewControllerDelegate {
    
    func initializeBuyWithMasterPass() {
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.dismissSafariViewController), name: NSNotification.Name(rawValue: "SAFARIVIEWCALLBACK"), object: nil);
        
        let callbackURL = "http%3A%2F%2Fsample.masterpass.informant.com.br%2Fredirect-ios.html"
        let checkoutId  = "95f1ea5373f64018a0f2577d119289cc"
        let cartId = "SomeUser-\(Date().timeIntervalSince1970 * 1000.0)"
        
        let merchantWebURL = "https://sandbox.masterpass.com/switchui/index.html?"
            + "allowedCardTypes=master%2Camex%2Cdiners%2Cdiscover%2Cjcb%2Cmaestro%2Cvisa"
            + "&callbackUrl=\(callbackURL)"
            + "&cartId=\(cartId)"
            + "&currency=BRL"
            + "&amount=\(1)"
            + "&suppress3Ds=true"
            + "&suppressShippingAddress=true"
            + "&checkoutId=\(checkoutId)"
            + "&checkoutVersion=v6"
        
        
        print(merchantWebURL)
        self.initializeSafariViewController(merchantWebURL)
    }
    
    
    func initializeSafariViewController(_ merchantWebUrl:String){
        self.safariView = SFSafariViewController(url: URL(string: merchantWebUrl)!)
        self.safariView?.delegate = self
        self.present(self.safariView!, animated: false) {
            print("Safari view is presented");
        }
    }
    
    @objc func dismissSafariViewController() {
        dismissSafariViewControllerWith {}
    }
    
    func dismissSafariViewControllerWith(completion: @escaping () -> Void){
        if let sViewController = safariView {
            sViewController.dismiss(animated: true, completion: {
                completion()
            });
        }
    }
    
    func checkoutCompletion(callbackURL url: URL) {
        
        var didDismiss = false
        
        let urlComponents = url.absoluteString.components(separatedBy: ";")
        for component in urlComponents {
            if component.contains("mpstatus") {
                if component.components(separatedBy: "=")[1].contains("success") {
                    for componentA in urlComponents {
                        if componentA.contains("oauth_token") {
                            let masterPassToken = componentA.components(separatedBy: "=")[1]
                            print("MasterPass token:")
                            print(masterPassToken)
                            dismissSafariViewControllerWith {
                                let alertController = UIAlertController(title: "Checkout completo", message: "A simulação de checkout foi um sucesso. Token recebido: \(masterPassToken)", preferredStyle: .alert)
                                alertController.addAction((UIAlertAction(title: "OK", style: .default, handler: { act in alertController.dismiss(animated: true, completion: {}) })))
                                self.present(alertController, animated: true, completion: {})
                            }
                            didDismiss = true
                            break
                        }
                    }
                    break
                } else {
                    dismissSafariViewControllerWith {
                        var title = "Aconteceu um erro"
                        var message = "O checkout não foi concluido"
                        if component.contains("mpstatus=cancel") {
                            title = "Checkout cancelado"
                            message = "O usuário cancelou o checkout"
                        } else {
                            title = "Aconteceu um erro"
                            message = "O checkout não foi concluido"
                        }
                        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
                        alertController.addAction((UIAlertAction(title: "OK", style: .default, handler: { act in alertController.dismiss(animated: true, completion: {}) })))
                        self.present(alertController, animated: true, completion: {})
                    }
                }
            }
        }
        if !didDismiss {
            dismissSafariViewControllerWith {}
        }
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        print("Did finish view controller ");
    }
    
}

extension ViewController {
    
    fileprivate static func masterpassButtonHTML() -> String {
        return "<style>html, body, #wrapper {height:100%;width:100%;margin:0;padding:0;border:0;}" +
            "#wrapper td {vertical-align:middle;text-align:center;}</style>" +
            "<body style=\"background-color: transparent;\">" +
            "<table id=\"wrapper\">" +
            "<tr>" +
            "<td>" +
            "<a href=\"inapp://checkout\"><img src=\"https://static.masterpass.com/dyn/img/btn/global/mp_chk_btn_147x034px.svg\"/></a>" +
            "</td>" +
            "</tr>" +
            "</table>" +
            "<script type=\"text/javascript\" src=\"https://sandbox.static.masterpass.com/integration/merchant.js\"></script>" +
        "</body>"
    }
    
}

